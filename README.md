# Hef4yrs

[View Site](https://wedding.happilyeverferrer.com/)

## Intro

A site to showcase our wedding day. Will slowly build upon this over time.

## Goals

- Create a one-pager to bring back the good memories of our wedding day.
- Continue to use and embrace Nuxt and Vue

## Tech Used

- Vue / Javascript
- HTML
- CSS
- Photoshop

## Services Used

- [GitLab](https://gitlab.com/) -- Git Repo
- [Netlify](https://netlify.com/) -- Static File Hosting and deployment
- [Cloudinary](http://cloudinary.com/) -- (POST Live) Image hosting with easy-to-use image transformations/optimizations.
