export const utilObserver = {
  methods: {
    imageObserver(entries, observer) {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          const el = entry.target;
          const elSrc = el.dataset.src;
          el.src = elSrc;
          if (entry.intersectionRatio >= 0.2) {
            el.classList.add("loaded");
            observer.unobserve(el);
          }
        }
      });
    },
    runObserver(target = ".lazy", options = 0) {
      const observerOptions = {
        threshold: options
      };
      const observer = new IntersectionObserver(
        this.imageObserver,
        observerOptions
      );
      let els = document.querySelectorAll(target);
      els.forEach(el => {
        observer.observe(el);
      });
    }
  }
};

export const getWindowHeight = {
  data() {
    return {
      siteHeight: ""
    };
  },
  methods: {
    getWindowHeight() {
      const innerHeight = self.innerHeight + "px";
      this.siteHeight = innerHeight;
    }
  },
  mounted() {
    this.getWindowHeight();
  }
};

export const getImagesInDir = {
  // data() {
  //   return {
  //     images: []
  //   };
  // },
  // created() {
  //   this.importAll(
  //     require.context(`../assets/${this.directory}`, true, /\.jpg$/)
  //   );
  // },
  // methods: {
  //   importAll(r) {
  //     r.keys().forEach(key =>
  //       this.images.push({ pathLong: r(key), pathShort: key })
  //     );
  //   }
  // }
};
