export default {
  mode: "spa",
  // mode: "universal",
  /*
   ** Headers of the page
   */
  server: {
    port: 4000, // default: 3000
    host: "0.0.0.0" // default: localhost
  },
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      },
      { name: "robots", content: "noindex" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    name: "cube-grid",
    color: "#98c2c2",
    height: "5px"
  },
  /*
   ** Global CSS
   */
  css: ["@/assets/scss/main.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: "~/plugins",
      ssr: false
    }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: ["@bazzite/nuxt-optimized-images", "nuxt-webfontloader"],
  optimizedImages: {
    optimizeImages: true
  },
  webfontloader: {
    // add Google Fonts as "custom" | workaround required
    custom: {
      families: [
        "Niconne:n4",
        "Nunito Sans:n3,n4,n7"
        // 'Lora:n4,n7'
      ],
      urls: [
        // for each Google Fonts add url + options you want
        // here add font-display option
        "https://fonts.googleapis.com/css?family=Niconne:400&display=swap",
        "https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,700&display=swap"
        // 'https://fonts.googleapis.com/css?family=Lora:400,700&display=swap'
      ]
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
};
