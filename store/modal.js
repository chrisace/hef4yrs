export const state = () => ({
  modalImg: "",
  modalOpen: false
});

export const mutations = {
  addImg(state, url) {
    state.modalImg = url;
    console.log("mutation: addImg");
  },
  openModal(state) {
    state.modalOpen = true;
    console.log("mutation: open modal");
  },
  closeModal(state) {
    state.modalOpen = false;
    console.log("mutation: close modal boolaha");
  },
  clearModalImage(state) {
    state.modalImg = "";
    console.log("mutation: clearing image");
  }
};

export const actions = {
  addImg({ dispatch, commit, getters, rootGetters }, url) {
    commit("addImg", url);
    commit("openModal");
    console.log("action: add img and open modal");
  },
  closeModal({ commit }) {
    commit("closeModal");
    commit("clearModalImage");
    console.log("action: close modal");
  }
};

export const getters = {
  modalImgUrl(state, getters, rootState) {
    return state.modalImg;
  },
  isModalOpen(state) {
    return state.modalOpen;
  }
};
